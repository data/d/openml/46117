# OpenML dataset: Dominick

https://www.openml.org/d/46117

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dominick dataset forecasting data, weekly data.

From the website:
-----
This dataset contains 115704 weekly time series representing the profit of individual stock keeping units from a retailer.

-----

It is not clear which of the 'movement' files from https://www.chicagobooth.edu/research/kilts/research-data/dominicks# was used. 
They also did not keep a track of the date.

There are 3 columns:

id_series: The identifier of a time series.

value: The value of the time series at 'time_step'.

time_step: The time step on the time series.

Preprocessing:

1 - Renamed columns 'series_name' and 'series_value' to 'id_series' and 'value'.

2 - Exploded the 'value' column.

3 - Created 'time_step' column from the exploded data.

4 - Defined 'id_series' as 'category' and casted 'value' to float.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46117) of an [OpenML dataset](https://www.openml.org/d/46117). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46117/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46117/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46117/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

